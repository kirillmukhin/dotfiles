#! /bin/bash

# Download and install spaceduck theme for konsole
if [[ -x "$(command -v konsole)" ]]; then
	echo "[✔] Command 'konsole' was found!"
	if [[ -f "$HOME/.local/share/konsole/spaceduck.colorscheme" ]]; then
		echo "[!] spaceduck colorscheme is already present"
	else
		wget --directory-prefix=$HOME/.local/share/konsole/ 'https://github.com/pineapplegiant/spaceduck-terminal/raw/main/spaceduck.colorscheme'
		if [[ -f "$HOME/.local/share/konsole/spaceduck.colorscheme" ]]; then
			echo "[✔] Spaceduck colorscheme successfully downloaded"
		else
			echo "[✖] [ERROR] failed to download the spaceduck colorscheme"
		fi
	fi
	if [[ -f "$HOME/.local/share/konsole/spaceduck.colorscheme" ]]; then
		konsoleprofile colors="spaceduck" #TODO that just changes current terminal session
		echo "[✔] Konsole profile was set"
	else
		echo "[✖] [ERROR] No theme found, can not set Konsole's color profile."
	fi
	else
		echo "[✖] Command 'konsole' was not found!"
fi
